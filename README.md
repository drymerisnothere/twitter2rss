<div id="table-of-contents">
<h2>&Iacute;ndice</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#sec-1">1. Twitter2RSS</a>
<ul>
<li><a href="#sec-1-1">1.1. English</a>
<ul>
<li><a href="#sec-1-1-1">1.1.1. About</a></li>
<li><a href="#sec-1-1-2">1.1.2. DISCLAIMER</a></li>
<li><a href="#sec-1-1-3">1.1.3. Requirements</a></li>
<li><a href="#sec-1-1-4">1.1.4. Git repository</a></li>
<li><a href="#sec-1-1-5">1.1.5. Installation</a></li>
<li><a href="#sec-1-1-6">1.1.6. Settings</a></li>
<li><a href="#sec-1-1-7">1.1.7. Web Interface</a></li>
<li><a href="#sec-1-1-8">1.1.8. Crontab</a></li>
<li><a href="#sec-1-1-9">1.1.9. License</a></li>
</ul>
</li>
<li><a href="#sec-1-2">1.2. Castellano</a>
<ul>
<li><a href="#sec-1-2-1">1.2.1. Acerca de</a></li>
<li><a href="#sec-1-2-2">1.2.2. DISCLAIMER</a></li>
<li><a href="#sec-1-2-3">1.2.3. Requisitos</a></li>
<li><a href="#sec-1-2-4">1.2.4. Repositorio git</a></li>
<li><a href="#sec-1-2-5">1.2.5. Instalación</a></li>
<li><a href="#sec-1-2-6">1.2.6. Configuración</a></li>
<li><a href="#sec-1-2-7">1.2.7. Interfaz Web</a></li>
<li><a href="#sec-1-2-8">1.2.8. Crontab</a></li>
<li><a href="#sec-1-2-9">1.2.9. Licencia</a></li>
</ul>
</li>
</ul>
</li>
</ul>
</div>
</div>


# Twitter2RSS<a id="sec-1" name="sec-1"></a>

## English<a id="sec-1-1" name="sec-1-1"></a>

### About<a id="sec-1-1-1" name="sec-1-1-1"></a>

twitter2rss searches for twitter's user data and creates a feed from it.

### DISCLAIMER<a id="sec-1-1-2" name="sec-1-1-2"></a>

This program searches the data directly on the twitter's website using a technique called scrapping, it does not use it's API. This means that any changes made to the website can bug this program. Each time it happens, will be fixed, but it probably will not work for a while.

### Requirements<a id="sec-1-1-3" name="sec-1-1-3"></a>

You need a version of Python and pip equal to or greater than 3.4 and two libraries:

-   [requests](https://pypi.python.org/pypi/requests) >= 2.9.0

-   [PyRSS2Gen](https://pypi.python.org/pypi/PyRSS2Gen) >= 1.1

It can be used with Python 3.2 (the version that comes with Debian Wheezy), but you have to change a line.

    # Line 30 is:
    HTMLParser .__ init __ (self, convert_charrefs = True)
    # Change to:
    HTMLParser .__ init __ (self)

### Git repository<a id="sec-1-1-4" name="sec-1-1-4"></a>

It's in two places:

-   <http://daemons.cf/cgit/twitter2rss>: the original repository

-   <https://notabug.org/drymer/twitter2rss/>: A mirror in which you can put your problems and suggestions for improvements

### Installation<a id="sec-1-1-5" name="sec-1-1-5"></a>

As any program written in Python, is it advisable to use a virtual environment (virtualenv), but that is user's choice. It can choosen from the following installation methods:

Installed using pip:

    $ su -c "pip3 install twitter2rss"

Using git:

    $ git clone git: //daemons.cf/twitter2rss
    # Or ...
    $ git clone https://notabug.org/drymer/twitter2rss

    $ cd twitter2rss
    $ su -c "pip3 install -r requirements.txt"
    $ su -c "python3 setup.py install"

### Settings<a id="sec-1-1-6" name="sec-1-1-6"></a>

This program has no options, so it should be easy to use. All it have to be done is to create a file in the directory where `twitter2rss` run and fill it with twitter user's names:

    $ cd PATH/TO/twitter2rss/
    # Edit user file. The following are just four examples
    $ echo -e "snowden\nioerror\neldiarioes\nlamarea_com" > twitter_users
    $ twitter2rss.py
    # Or if you just want to add or update one
    $ twitter2rss.py snowden

In addition, it's possible change the threads used by the program. If they're increased and the machine is powerful enough, it will run faster.

By default it uses two threads in the implementation of the program. You can climb more, if the machine supports it, to five threads. From ten threads, the speed increase is no longer noticed, so is not worth it.

### Web Interface<a id="sec-1-1-7" name="sec-1-1-7"></a>

This repository can be cloned directly into the root of the web server and run as is with the standard configuration of PHP. For the nginx virtual host, you can use the file called `nginx_virtualhost` file.

This interface does two things: put the Twitter user in the file `twitter_users` and run a Python function in which only creates the feed of that user.

### Crontab<a id="sec-1-1-8" name="sec-1-1-8"></a>

The `twitter2rss` preferred execution mode is using crontab. If added the following, should apply:

    $ crontab -e
    # A file is open and gets the following
    */5 * * * * cd /var/www/twitter2rss && twitter2rss.py

Like this, it runs every five minutes. Is recommended this low execution time as tweets could be lost otherwise. It has to be remembered that it's important to run it in the directory where the file `twitter_users` was created, since it's there where it will try to find it.

### License<a id="sec-1-1-9" name="sec-1-1-9"></a>

    This program is free software: you can redistribute it and / or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, Either version 3 of the License, or
    (At your option) any later version.

    This program is distributed in the hope That it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    With This program along. If not, see <http://www.gnu.org/licenses/>.

## Castellano<a id="sec-1-2" name="sec-1-2"></a>

### Acerca de<a id="sec-1-2-1" name="sec-1-2-1"></a>

twitter2rss busca los datos de usuarios de twitter y crea un feed a partir de ello.

### DISCLAIMER<a id="sec-1-2-2" name="sec-1-2-2"></a>

Este programa busca los datos directamente en la web de Twitter mediante una técnica llamada scrapping, no usa su API. Esto quiere decir que cualquier cambio que hagan a la web puede fastidiar el funcionamiento de este programa. Cada vez que pase, se procurará arreglarlo, pero es probable que durante un tiempo no funcione.

### Requisitos<a id="sec-1-2-3" name="sec-1-2-3"></a>

Necesita una versión de Python y pip igual o superior a la 3.4 y dos librerias:

-   [requests](https://pypi.python.org/pypi/requests) >= 2.9.0

-   [PyRSS2Gen](https://pypi.python.org/pypi/PyRSS2Gen) >= 1.1

Se puede usar con Python 3.2 (la versión que trae Debian Wheezy), pero hay que modificar una línea.

    # Linea 30, es:
    HTMLParser.__init__(self, convert_charrefs=True)
    # Cambiar por:
    HTMLParser.__init__(self)

### Repositorio git<a id="sec-1-2-4" name="sec-1-2-4"></a>

Está en dos sitios:

-   <http://daemons.cf/cgit/twitter2rss>: el repositorio original

-   <https://notabug.org/drymer/twitter2rss/>: un mirror, en el que se pueden poner los problemas y sugerencias de mejoras

### Instalación<a id="sec-1-2-5" name="sec-1-2-5"></a>

Cómo con cualquier programa escrito en Python, es recomendable usar un entorno virtual (virtualenv), pero eso queda a elección del usuario. Se puede escoger entre los siguientes métodos de instalación:

Instalar usando pip:

    $ su -c "pip3 install twitter2rss"

Usando git:

    $ git clone git://daemons.cf/twitter2rss
    # O ...
    $ git clone https://notabug.org/drymer/twitter2rss

    $ cd twitter2rss
    $ su -c "pip3 install -r requirements.txt"
    $ su -c "python3 setup.py install"

### Configuración<a id="sec-1-2-6" name="sec-1-2-6"></a>

Este programa no tiene ninguna opción, por lo que debería ser sencillo de usar. Lo único que hay que hacer es crear un archivo en el directorio en el que se ejecute `twitter2rss` y llenarlo con nombres de usuarias de Twitter:

    $ cd RUTA/A/twitter2rss/
    # editar fichero de usuarios. Lo siguiente son sólo cuatro ejemplos
    $ echo -e "snowden\nioerror\neldiarioes\nlamarea_com" > twitter_users
    $ twitter2rss.py
    # O si sólo se quiere añadir o actualizar uno
    $ twitter2rss.py snowden

Además, se pueden modificar los hilos que usa el programa. Si se aumentan y la máquina es suficientemente potente, se ejecutará más rápido.

Por defecto usa dos hilos en la ejecución del programa. Se puede subir a más, si la máquina lo soporta, a cinco hilos. A partir de los diez hilos ya no se nota el incremento de velocidad, por lo que no vale la pena.

### Interfaz Web<a id="sec-1-2-7" name="sec-1-2-7"></a>

Este repositorio se puede clonar directamente en la raiz del servidor web y funcionará tal cual con la configuración estándar de PHP. Para el virtual host de nginx, se puede usar el fichero llamado `nginx_virtualhost`.

Esta interfaz hace dos cosas, meter la usuaria de Twitter en el archivo `twitter_users` y ejecutar una función de Python en la que sólo crea el feed de esa usuaria.

### Crontab<a id="sec-1-2-8" name="sec-1-2-8"></a>

El modo recomendado de ejecución de `twitter2rss` es usando el crontab. Con poner lo siguiente, deberia valer:

    $ crontab -e
    # Se abrirá un archivo y se mete lo siguiente
    */5 * * * * cd /var/www/twitter2rss && twitter2rss.py

Así se ejecuta cada cinco minutos. Se recomienda este tiempo de ejecución tan bajo por que se podrian perder tweets de no ser así. Hay que recordar que es importante que se ejecute en el directorio en el que se ha creado el archivo `twitter_users`, ya que es ahí dónde lo buscará.

### Licencia<a id="sec-1-2-9" name="sec-1-2-9"></a>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
