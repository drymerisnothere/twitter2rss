 <!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Twitter2rss</title>
<link rel="stylesheet" type="text/css" href="twitter2rss.css" />
</head>
<body>

<?php
define("FEED_LIST", "twitter_users");
$user = "";
$userErr = "";
header('Content-Type: text/html; charset=utf-8');
$locale='en_EN.UTF-8';
setlocale(LC_ALL,$locale);
putenv('LC_ALL='.$locale);

function invalid_characters($string) {
    if (!preg_match("/^[a-zA-Z0-9_]*$/",$string)) {
        return true;
    }
    else {
        return false;
    }
}

function user_exist($string) {
    $fh = fopen(FEED_LIST, 'r') or die("I couldn't read the file.");
    $res = false;
    while (!feof($fh) && !$res) {
        $line = fgets($fh, 4096);
        if ( strcasecmp($string . "\n" , $line) == 0 ) {
            $res = true;
        }
    }
    fclose($fh);
    return $res;
}

function on_twitter($string) {
    $tURL = "https://twitter.com/" . $string;
    $headers = get_headers($tURL);
    if ( intval(substr($headers[0], 9, 3)) < 400 ) {
        return true;
    } else {
        return false;
    }
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["user"])) {
        $userErr = "It's necessary to add a user.";
    } elseif (invalid_characters($_POST["user"])) {
        $userErr = "Invalid characters.";
    } elseif (user_exist($_POST["user"])) {
        $userErr = "The user was added before.\nYou can find it <a href='http://$_SERVER[HTTP_HOST]/feeds/$_POST[user].xml'>here</a>";
    } elseif (!on_twitter($_POST["user"])) {
        $userErr = "The user doesn't exist on Twitter.";
    } else {
        $fh = fopen(FEED_LIST, 'a') or die("I couldn't write in the file.");
        $stringData = $_POST["user"] . "\n";
        fwrite($fh, $stringData);
        fclose($fh);
        $user = $_POST["user"];
        exec("python3 twitter2rss.py ".escapeshellarg($user));
    }
}
?>

<div class="content">
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <h1>twitter2rss<span>Add Twitter users and receive it\'s feed</span></h2>
        <p>
        <label>
            <span>User:</span>
            <input type="text" name="user" value="<?php echo $user;?>">
       </label>
       <label>
           <span>&nbsp;</span>
           <input class="button" type="submit" name="submit" value="Añadir">
      </label></p>
    </form>

    <?php if (!empty($userErr)): ?>
        <span class="error"><?php echo $userErr;?></span>
    <?php endif; ?>
    <?php if (!empty($user)): ?>
        <span class="success">User <?php echo $user;?> added.<br />
        <?php echo "<a href='http://$_SERVER[HTTP_HOST]/feeds/$_POST[user].xml'>Here</a>"; ?></span>
    <?php endif; ?>
    <br>
<div>
    <!-- If twitter2rss is in a folder, you should add that folder behind http://$_SERVER[HTTP_HOST]/ -->
    <div margin="10px" float="left"><?php echo "<a href='feeds'>Existing feeds</a>" ?></div>
    <div text-align="center" float="center"><?php echo "<a href='http://daemons.cf/cgit/twitter2rss/about/'>About</a>"?></div>
    <div text-align="right" float="right"><?php echo "<a href='http://daemons.cf/cgit/twitter2rss'>Git Repository</a>" ?></div>
</div>
</div>
</body>
</html>
